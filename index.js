let credentials = require('./credentials.json');
let settings = require('./settings.json');
let Snooper = require('reddit-snooper');
let cheerio = require('cheerio');
const PS = require('promise-stack');
const fetch = require("node-fetch");
const fs = require('fs');
const http = require('http');

// Just  make heroku happy
if (settings.hack_heroku) {
    const PORT = process.env.PORT || 3000;
    http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write('Heroku this is not a website so stop killing my application with R10 codes thank you.');
    res.end();
    }).listen(PORT); // Prevent R10 shutdown
    setInterval(() => {
        fetch(settings.heroku_domain);
    }, 20 * 60 * 1000); // Prevent IDLING shutdown (not enough activity)
}


// Declare variables
const ps = new PS();
const fetchInit = {
    mode: 'cors',
    headers: {
        'User-Agent': 'curl/7.64.1',
        'Accept': '*/*'
    }
};

const domains = [
    'imgur.com', 
    'i.redd.it', 
    'i.reddituploads.com',
    'i.imgur.com',
    'i.gyazo.com',
    'media.giphy.com',
    'v.redd.it',
    'gfycat.com'
];

const banned_subreddits = [
    'r/AdviceAnimals',
    'r/ColorizedHistory'
];

const log = (thing) => {
    let value = thing;
    if(typeof thing !== 'string')
        value = JSON.stringify(thing);

    // They're all saying 'fuck php' nevertheless sometimes I miss it
    const date = (new Date()).getHours() + ':' + (new Date()).getMinutes() + ':' + (new Date()).getSeconds() + "\t";
    console.log(date, thing);
}

function trim_to(str, max = 35) {
    if(str.length > max) {
        return str.substring(0, max - 3) + '...';
    }
    return str;
}

// user-agent is optional
snooper = new Snooper(
    {
        // credential information is not needed for snooper.watcher
        username: credentials.username,
        password: credentials.password,
        app_id: credentials.app_id,
        api_secret: credentials.app_secret,
        user_agent: credentials.user_agent,

        automatic_retries: true, // automatically handles condition when reddit says 'you are doing this too much'
        api_requests_per_minute: 60 // api requests will be spread out in order to play nicely with Reddit
    }
);

const options = {
    listing: 'hot', // 'hot' OR 'rising' OR 'controversial' OR 'top_day' OR 'top_hour' OR 'top_month' OR 'top_year' OR 'top_all'
    limit: 50 // how many posts you want to watch? if any of these spots get overtaken by a new post an event will be emitted, 50 is 2 pages
}


// main
snooper.watcher.getListingWatcher('all', options)
    .on('item', function(item) {
        const domain = item.data.domain;
        const karmaUrl = 'http://karmadecay.com' + item.data.permalink;
        const title = item.data.title;
        const threadname = item.data.name;
        const subreddit_name = item.data.subreddit_name_prefixed;

        if(!domains.includes(domain)) {
            //console.log("Does not include domain: ", domain);
            return;
        }

        if(banned_subreddits.includes(subreddit_name) || subreddit_name.toLowerCase().includes("twitter")) {
            // Twitter subreddits or generic memes subreddits or colorized subreddits
            return;
        }

        // We do not want to query everything at the same time so we're using an underground npm
        // package to chain those fetch calls one after the other
        log('+ Adding fetch to the stack: ' + trim_to(title) + ' - ' + subreddit_name);
        let promise = () => fetch(karmaUrl, fetchInit)
            .then((r) => {
                if (r.status !== 200) {
                    log('✖ Request to karmadecay ended in a HTTP ' + r.status)
                    r.text().then((body) => {log(trim_to(body, 500))});
                    throw new Error('Karmadecay failed to answer');
                }
                return r;
            })
            .then(r => r.text())
            .then((html) => {
                const $ = cheerio.load(html);
                let explanation = '??';
                let ret = {enoughSpam: false};

                try {
                    if ($('tr.ns').length > 0) {
                        explanation = 'No similar images found';
                    }
                    else if ($("#contextInfo").length === 0) {
                        explanation = 'No image found for this thread';
                    } else {
                        const text = $("#contextInfo")[0].nextSibling.data.trim();
                        const numsameones = parseInt(/\d+/.exec(text)[0]);
                        if(numsameones >= 20) {
                            log('! ' + title + ' has a high amount of reposts: ' + numsameones);
                            ret.enoughSpam = true;
                            ret.links = $("tr.result div.title a").map((i, e) => ({link: e.attribs.href, title: e.children[0].data})).toArray();
                        }
                        explanation = 'Has ' + numsameones + ' similar threads.';
                    }

                    log('✔ ' + title + ': ' + explanation);
                    return ret;
                } catch (e) {
                    log('✖ Failed to parse HTML: ' + e.message + ' (' + title + ')');
                    fs.writeFileSync('errors/' + title + '.html', html + e.message);
                    ret = {enoughSpam: false};
                }

                return ret;
            }).catch(() => {
                log('✖ Stopping further work on ' + trim_to(title));
                return {enoughSpam: false};
            });

        ps.set(promise).then((r) => {
            if (r.enoughSpam) {
                snooper.api.get('/user/' + credentials.username + '/comments', {
                    limit: 50
                }, function (err, statusCode, data) {
                    if(err) {
                        log('✖ Failed to get bot\'s comments');
                        return;
                    }
                
                    const hasCommented = data.data.children.some((i) => {
                        log('If those are equal, then bot commented: ' + i.data.link_id + ' -- ' + threadname); // TODO remove
                        return i.data.link_id === threadname;
                    });

                    if (hasCommented) {
                        log('✔ The bot already left a comment on this thread, no need to post again');
                        return;
                    }

                    log('The bot will leave a comment on the thread'); // TODO REMOVE

                    // Let's prepare the message text
                    let message = "I can make make mistakes, but I think I've seen this a good amount of times on reddit already.\n\n";
                    r.links.forEach((e, i) => {
                        message += i+1 + ". [" + e.title + "](" + e.link + ")\n\n";
                    });
                    message += 'I\'m a bot, if you have questions ask this idiot -> u/yhu420';


                    snooper.api.post("/api/comment", {
                        api_type: "json",
                        text:     message,
                        thing_id: threadname
                    }, function (err, statusCode, data) {
                        if (err) {
                            log('✖ An error occurred while posting the comment on ' + title)
                            return;
                        }

                        log('✔ Comment posted on ' + title);
                    })
                })
            }
        })
    })
    .on('error', console.error);